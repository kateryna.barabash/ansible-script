# Ansible script

1. Install python-pexpect on target machine.
2. Change patches in "patch_dict.yml".
3. Define each patch metadata in “vars/patch_dictionary/patch_dict.yml".
4. Download repo from git to VM.
5. Run ansible-playbook path_to_playbook -k.

